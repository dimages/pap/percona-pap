REGISTRY=registry.gitlab.com
USERNAME=paymastergroup
TAG=4

login:
	docker login -u ${USERNAME} ${REGISTRY}


new-tag: build tag push

build:
	docker build -t ${REGISTRY}/${USERNAME}/platform-percona:${TAG} -f Dockerfile .

tag:
	docker tag ${REGISTRY}/${USERNAME}/platform-percona:${TAG} ${REGISTRY}/${USERNAME}/platform-percona:latest

push:
	docker push ${REGISTRY}/${USERNAME}/platform-percona:${TAG} && \
	docker push ${REGISTRY}/${USERNAME}/platform-percona:latest


pull:
	docker pull ${REGISTRY}/${USERNAME}/platform-percona:latest

run:
	docker run -it ${REGISTRY}/${USERNAME}/platform-percona:${TAG} bash