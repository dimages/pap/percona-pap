FROM percona:latest
COPY config/custom.cnf /etc/mysql/conf.d/custom.cnf
RUN chmod 600 /etc/mysql/conf.d/custom.cnf
VOLUME ["/var/lib/mysql"]